'use strict';

export default function appConfig(uiSelectConfig, NotificationProvider) {
	/**
	 * Provider to access temporary state in ui-router state resolve
	 * Uses $transitions instead of $rootScope.$on('$stateChangeStart'...)
	 * as those events are deprecated already
	 */
	
  	/**
  	 * Angular ui-notification provider 
  	 */
  	 NotificationProvider.setOptions({
        delay: 10000,
        replaceMessage: true,
        startTop: 20,
        startRight: 20,
        verticalSpacing: 20,
        horizontalSpacing: 20,
        positionX: 'right',
        positionY: 'top'
    });

  	/**
  	 * Angular ui select2 theme settings
  	 */
	uiSelectConfig.theme = 'select2';
	
}