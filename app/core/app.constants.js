'use strict';

export const CONST = {
	"ENV": {
		/**
		 * WORDPRESS API
		 *
		 * Global WP Options
		 */
		"API_BASE": "backend/wp-json", 

		/**
		 * WP data
		 */
		"API": "/backend/wp-json/wp/v2"
	},
	"STATIC": {
		"defaultThumb": "/app/assets/img/default-thumb.png"
	},
	"GOOGLE_MAP_KEY": "AIzaSyB03tAwefwr3O2O8EskKilDKWVPz9_MEaU",
	"WP": {
		"HOME_PAGE_ID": 33
	}
}