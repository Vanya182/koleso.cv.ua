'use strict';

export default function run($rootScope, $state, $stateParams) {
	$rootScope.$state = $state;
  	$rootScope.$stateParams = $stateParams;

  	$rootScope.$on('$stateChangeStart', function(event, toUrl, fromUrl) {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
	});
}