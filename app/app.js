'use strict';

/**
 * App dependencies
 */
import * as angular from "angular";
import uirouter from 'angular-ui-router/release/angular-ui-router.min.js';
import uiSelect from 'ui-select';
import angularSanitize from 'angular-sanitize';
// https://github.com/alexcrack/angular-ui-notification
import notification from 'angular-ui-notification';
import angularLoadingBar from 'angular-loading-bar';
import ngFileUpload from 'ng-file-upload';
import ngFileModel from 'ng-file-model';
import 'ui-router.grant';

/**
 * App config
 */
import run from 'core/app.run';
import config from 'core/app.config';
import routes from 'core/app.routes';
/**
 * App constants
 */
import {_fbRoutes} from 'components/shared/constants/firebaseRoutes';
/**
 * App components
 */
import home from 'components/home';
import contacts from 'components/contacts';
import team from 'components/team';
import downloadRelevantStock from 'components/download-relevant-stock';
import downloadRelevantUnderTheOrder from 'components/download-relevant-under-the-order';
import user from 'components/user';

/**
 * App shared components
 */
import {headerConfig} from 'components/shared/components/header';
import {footerConfig} from 'components/shared/components/footer';
import {imgAsyncConfig} from 'components/shared/components/img-async';
import {fancyboxConfig} from 'components/shared/components/fancybox';
import {imgErrCheckConfig} from 'components/shared/components/imgErrCheck';
/**
 * App shared resources
 */
import userRes from 'components/shared/resources/userRes';
import firebaseRes from 'components/shared/resources/firebaseRef';

/**
 * App shared filters
 */
import sceTrustAsHtmlConfig from 'components/shared/filters/sceTrustAsHtml';


/**
 * App bootstraping
 */
angular
	.module('koleso', [
		/**
		 * Dependencies
		 */
		uirouter,
		uiSelect,
		angularSanitize,
		notification,
		angularLoadingBar,
		ngFileUpload,
		'ui.router.grant',
		'ng-file-model',
		/**
		 * App components
		 */
		home,
		team,
		user,
		contacts,
		downloadRelevantStock,
		downloadRelevantUnderTheOrder
	])
	.config(config)
	.config(routes)
	.run(run)
	.constant('_fbRoutes', _fbRoutes)
	.component('headerComponent', headerConfig)
	.component('footerComponent', footerConfig)
	/**
	 * Thi one is a directive not a component as we want to 
	 * avoid extra component wrapper element 
	 */
	.directive('imgAsyncComponent', imgAsyncConfig)
	.directive('fancybox', fancyboxConfig)
	.directive('imgErrCheck', imgErrCheckConfig)
	.service('UserResources', userRes)
	.service('_fb', firebaseRes)
	.filter('sceTrustAsHtml', sceTrustAsHtmlConfig)

