'use strict';

import imgAsyncController from './img-async.controller';

export function imgAsyncConfig() {
	return {
		restrict: 'A',
	    controller: imgAsyncController,
	    scope: {
	    	media: '='
	  	}
	}
};

   
