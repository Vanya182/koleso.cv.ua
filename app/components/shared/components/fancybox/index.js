'use strict';

require('fancybox')($);

export function fancyboxConfig() {
    return {
        restrict: 'A',
        link: (scope, element, attrs) => {
        	console.log(scope.useFancybox);
            if (scope.useFancybox) {
                $(element).fancybox({
                    'overlayShow': false,
                    'cyclic': false,
                    'showNavArrows': false
                });
            }
        },
        scope: {
            useFancybox: '<'
        }
    }
};