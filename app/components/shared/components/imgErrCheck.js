 'use strict';

 export function imgErrCheckConfig() {
     return {
         restrict: 'A',
         link: function(scope, element, attrs) {
             element.bind('error', function() {
                 if (attrs.src != attrs.errSrc) {
                     // To set backup src from element attr
                     // attrs.$set('src', attrs.errSrc);
                     // Just static custom thumb
                     attrs.$set('src', '/app/assets/img/default-thumb.png');
                 }
             });
         }
     }
 };