'use strict'

import {CONST} from 'core/app.constants';

/**
 * Course model to filter redundant WP data
 */
class Course{
  constructor(course){
    return {
        id: course.id,
        title: course.title.rendered,
        slug: course.slug,
        thumbID: course.featured_media,
        excerpt: course.excerpt.rendered,
        color: course.acf.color,
        menu_order: course.menu_order,
        durationInHours: course.acf.duration_in_hours,
        startDate: course.acf.start_date,
        price: course.acf.price,
        discountPrice: course.acf.discount_price
    }
  }
} 

export default class courseRes {
	constructor($http, $filter){
		this._$http = $http;
		this._$filter = $filter;
	}	

  getCourse(id){
    return this._$http.get(`${CONST.ENV.API}/courses/${id}`).then(res => {
      let course = res.data;
      return new Course(course);
    }).catch(err => {
      console.error(err);
    });
  }
  
  getCoursesByParentID(id){
    return this._$http.get(`${CONST.ENV.API}/courses/?parent=${id}`).then(res => {
      let courses = res.data;
      let response = [];
      response = courses.map(course => {
        return new Course(course);
      })
      return response;
    }).catch(err => {
      console.error(err);
    });
  }
}

