'use strict'

export default class userRes {
    constructor($rootScope, _fb, $q) {
        this._$rootScope = $rootScope;
        this._fb = _fb;
        this._$q = $q;
    }

    getUser() {
        return firebase.auth().currentUser;
    };

    isUserAdmin() {
        // TODO rewrite to $q
        return new Promise((resolve, reject) => {
            if (this.getUserToken()) {
                this._fb.getAuthUsers().on('value', data => {
                    let authUsers = Object.keys(data.val()).map(key => {
                        return key
                    });
                    let userUid = this.getUserToken();
                    if (authUsers.indexOf(userUid) >= 0) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                }, err => {
                    reject(err);
                });
            } else {
                resolve(false);
            }
        });
    }

    loginViaPassword(credentials) {
        return firebase.auth().signInWithEmailAndPassword(credentials.username, credentials.password);
    };

    getUserData(){
        let uid = this.getUserToken();
        if (uid){
             return this._fb.getRegisteredUserData(uid).once('value');
        }else{
            return this._$q.when(null);
        }
    }

    authWithToken(token) {
        alert(token);
        return firebase.auth().signInWithCustomToken(token);
    }

    registerUser(userData) {
        return firebase.auth().createUserWithEmailAndPassword(userData.email, userData.password);
    };

    clearUser() {
        this._$rootScope.$broadcast('user-was-logged-out');
        return firebase.auth().signOut();
    };

    getUserToken() {
        let localStorageKeys = Object.keys(localStorage);
        // Looking for needed local storage firebase auth key
        let subString = "firebase:authUser";
        for (let i = 0; i < localStorageKeys.length; i++) {
            if (localStorageKeys[i].indexOf(subString) > -1)
                return JSON.parse(localStorage.getItem(localStorageKeys[i])).uid;
        }
        return null
    };

    setUserToken(token) {
        return localStorage.setItem('authToken', token);
    };
}