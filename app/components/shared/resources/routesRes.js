'use strict'

import {CONST} from 'core/app.constants';

/**
 * Route model to filter redundant WP data
 */
class Route{
  constructor(route){
    return {
        id: route.id,
        title: route.title.rendered,
        slug: route.slug,
        thumbID: route.featured_media,
        excerpt: route.excerpt.rendered,
        color: route.acf.color,
        menu_order: route.menu_order
    }
  }
} 

export default class routesRes {
	constructor($http, $sce, $filter){
		this._$http = $http;
		this._$sce = $sce;
		this._$filter = $filter;
	}	

	getRoutes(){
		return this._$http.get(`${CONST.ENV.API}/courses/?per_page=100`).then(res => {
  		let courses = res.data;
  		/**
  		 * If course is a parent one (a branch) it has 
  		 * no parents so parent key is set to "0"
  		 */
  		let routes = courses
  		.filter(course => {
  			return course.parent == 0;
  		})
  		.map(course => {
  			return new Route(course);
  		});
  		return this._$filter('orderBy')(routes, 'menu_order');
  	}).catch(err => {
  		console.error(err);
  	});
	}

}

