import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './team.routes';
import teamController from './team.controller';

export default angular.module('app.team', [uirouter])
	.config(routing)
	.controller('teamController', teamController)
	// We need this as we need to export module name for angular DI
	.name;