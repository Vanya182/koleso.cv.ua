'use strict';

import template from './download-relevant-stock.html';

export default function routes($stateProvider) {
  $stateProvider
    .state('downloadRelevantStock', {
  		url: '/download-relevant-in-stock',
      	template: template,
      	controller: 'downloadRelevantStockController',
      	controllerAs: 'downloadRelevantStockCtrl',
      	pageTitle: ''
    });
}
