import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './download-relevant-stock.routes';
import downloadRelevantStockController from './download-relevant-stock.controller';

export default angular.module('koleso.downloadRelevantStock', [uirouter])
	.config(routing)
	.controller('downloadRelevantStockController', downloadRelevantStockController)
	// We need this as we need to export module name for angular DI
	.name;