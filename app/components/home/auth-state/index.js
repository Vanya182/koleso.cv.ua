'use strict';

import template from './auth-state.html';
import authStateController from './auth-state.controller';

export const authStateConfig = {
    template: template,
    controller: authStateController,
    controllerAs: 'authStateCtrl'
};

