'use strict';

export default class authStateController{
    constructor($scope, $rootScope, $timeout, $state, cfpLoadingBar, Notification, UserResources){
            this._$scope = $scope;
            this._$rootScope = $rootScope;
            this._$timeout = $timeout;
            this._$state = $state;
            this.cfpLoadingBar = cfpLoadingBar;
            this.Notification = Notification;
            this.UserResources = UserResources;

            // Set up listener to track user logged out event
            this.isUserSignedIn = this.UserResources.getUserData();            
            this._$scope.$on('user-was-logged-out', (event, args)=>{ 
                this.userData = null;
            });
            this.userData = null;
            this.isUserAdmin = false;
            this.loadDataCheck = false;
            this.getProfileData();
        }
        forceUpdateView(){
            this._$timeout(() => {
                this._$scope.$apply();
            })
        }
        logout(){
            this.UserResources.clearUser().then(()=>{
                this.isUserAdmin = false;
                this.userData = null;
                this.Notification.success(
                'Вы вышли с системы');
                this._$state.go('home');
            }).catch(()=>{
                this.Notification.error('Возникла ошибка');
            })
        }
        getProfileData(){
            this.UserResources.getUserData()
            .then(data=>{
                let userData = data && data.val();
                if (! userData){
                    this.UserResources.isUserAdmin().then(res => {
                        this.isUserAdmin = res;
                        this.forceUpdateView();
                    }).catch(err => {
                        this.Notification.error(err);
                    })
                }
                this.userData = userData;
                this.forceUpdateView();
                // this.userData = data;
                // if (this.userData){
                    // this.logoData = this.userData.username.toUpperCase().trim().replace(/\s+/g, '').substring(0,2);
                // }
                // this.loadDataCheck = true;
            }).catch(err=>{
                this.Notification.error(err);
            })
        }
}

