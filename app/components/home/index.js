import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './home.routes';
import homeController from './home.controller';

import {authStateConfig} from './auth-state';

export default angular.module('koleso.home', [uirouter])
	.config(routing)
	.controller('homeController', homeController)
	.component('authState', authStateConfig)
	// We need this as we need to export module name for angular DI
	// .controller('authStateController', authStateController)
	.name;