'use strict'


export default class HomeController {
    constructor(_fb, $scope, cfpLoadingBar, Notification, $timeout) {
        this._fb = _fb;
        this._$scope = $scope;
        this.cfpLoadingBar = cfpLoadingBar;
        this.Notification = Notification;
        this._$timeout = $timeout;



        this.showLoadingSpinner = true;
        this.isDataLoaded = false;


        this.pagesShown = 1;
        this.pageSize = 10;
        this.searchInit();
        this.trackSearchInputChange();





        this.model = {}
        this.itemArray = [{
            id: 1,
            name: 'фильтр № 1'
        }, {
            id: 2,
            name: 'фильтр № 2'
        }, {
            id: 3,
            name: 'фильтр № 3'
        }, {
            id: 4,
            name: 'фильтр № 4'
        }, {
            id: 5,
            name: 'фильтр № 5'
        }, ];

        this.selected = {
            value: this.itemArray[0]
        };
    }


    paginationLimit(data) {
        return this.pageSize * this.pagesShown;
    };

    hasMoreItemsToShow() {
        if (this.flowersId) {
            return this.pagesShown < (this.flowersId.length / this.pageSize);
        }
    };

    showMoreItems() {
        this.pagesShown = this.pagesShown + 1;
        this.search();
    };

    searchInit() {
        this.cfpLoadingBar.start();
        this._fb.getProducts().then(doc => {
          // console.log(doc);
          doc.docs.map(item => {
            console.log(item.data());
          })
            // this.model.products = doc.data();
            this.cfpLoadingBar.complete();
            this._$timeout(() => {
                this._$scope.$apply();
            });
        }).catch(err => {
            this.cfpLoadingBar.complete();
            this.Notification.error(err);
        })
    }

    trackSearchInputChange() {
        this.pagesShown = 1;
        this._$scope.$on('filterSearchData', (event, data) => {
            let searchKey = data.searchKey;
            this.filterSearchResults(searchKey);
        })
    }

    search(){

    }



}