'use strict';

import template from './download-relevant-under-the-order.html';

export default function routes($stateProvider) {
  $stateProvider
    .state('downloadRelevantUnderTheOrder', {
  		url: '/download-relevant-under-the-order',
      	template: template,
      	controller: 'downloadRelevantUnderTheOrderController',
      	controllerAs: 'downloadRelevantUnderTheOrderCtrl',
      	pageTitle: ''
    });
}
