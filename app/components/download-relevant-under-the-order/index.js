import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './download-relevant-under-the-order.routes';
import downloadRelevantUnderTheOrderController from './download-relevant-under-the-order.controller';

export default angular.module('koleso.downloadRelevantUnderTheOrder', [uirouter])
	.config(routing)
	.controller('downloadRelevantUnderTheOrderController', downloadRelevantUnderTheOrderController)
	// We need this as we need to export module name for angular DI
	.name;