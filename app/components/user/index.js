'use strict';

import angular from 'angular';
import uirouter from 'angular-ui-router';

/**
 * Page login
 */
import loginRouting from './login/login.routes';
import loginController from './login/login.controller';

/**
 * Page register
 */
import registerRouting from './register/register.routes';
import registerController from './register/register.controller';

/**
 * Page profile
 */
import profileRouting from './profile/profile.routes';

export default angular.module('koleso.user', [])
	.config(loginRouting)
	.controller('loginController', loginController)

	.config(registerRouting)
	.controller('registerController', registerController)

	.config(profileRouting)
	// We need this as we need to export module name for angular DI
	.name;