'use strict';

export default class profileController {
    constructor(_fb, $state, $timeout, $scope, $http, cfpLoadingBar, Notification, UserResources) {
        this._fb = _fb;
        this._$timeout = $timeout;
        this._$scope = $scope;
        this._$http = $http;
        this.UserResources = UserResources;
        this.cfpLoadingBar = cfpLoadingBar;
        this.Notification = Notification;
        this.uid = UserResources.getUserToken();
        this.userData = null;      
        this.uploadedFilesLength = null;
        this.getUserData();
    }

    updateProfile() {
        this._$http({
            method: 'POST',
            url: '/delete-folder',
            data: {
                userFolderPath: this.userData.userFolderPath
            }
        }).then(() => {
            return this._$http({
                method: 'POST',
                url: '/upload-to-folder',
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: function(data) {
                    let formData = new FormData();
                    formData.append("uid", data.uid);
                    if (data.files && data.files.length > 0) {
                        for (let i = 0; i < data.files.length; i++) {
                            formData.append("file" + i, data.files[i]);
                        }
                    }
                    return formData;
                },
                data: {
                    uid: this.uid,
                    files: this.userData.files
                }
            })
        })
        .then((filesUploadRes) => {
            this.userData.files = filesUploadRes.data;
            this._fb.getRegisteredUserData(this.uid).update(this.userData)
        }).then(res => {
            this.Notification.success('Анкетные данные были обновлены');
        }).catch(err => {
            console.log(err);
            this.Notification.error('Возникла ошибка');
        })
    }

    getUserData() {
        this.cfpLoadingBar.start();
        this._fb.getRegisteredUserData(this.uid).once('value').then(res => {
            this.userData = res.val();
            this._$timeout(() => {
                this._$scope.$apply();
            })
            this.cfpLoadingBar.complete();
        }).catch(err => {
            this.cfpLoadingBar.complete();
            this.Notification.error('Возникла ошибка');
        })
    }

    resetUserActions() {
        this._$scope.registerForm.$setPristine();
        this.userData = this.setUserData();
        this.isUserAgreedCheck = false;
    }

    fileUpdated($files) {
        this.uploadedFilesLength = $files.length;
    }

    resetFiles() {
        this.uploadedFilesLength = 0;
        this.userData.files = null;
    }


}