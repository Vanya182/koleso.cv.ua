'use strict';

import template from './profile.html';
import controller from './profile.controller.js';

export default function routes($stateProvider) {
  $stateProvider
    .state('profile', {
  		url: '/profile',
      	template: template,
      	controller: controller,
      	controllerAs: 'profileCtrl',
      	pageTitle: ''
    });
}
