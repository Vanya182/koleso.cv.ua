'use strict';

import notification from 'angular-ui-notification';

export default class registerController {
    constructor($http, $timeout, $scope, Notification) {
        this._$http = $http;
        this._$timeout = $timeout;
        this._$scope = $scope;
        this.Notification = Notification;
        this.userData = this.setUserDataForTesting();
        this.emailFormatRegex = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        this.isUserAgreedCheck = false;
        this.uploadedFilesLength = null;
    }

    setUserData() {
        return {
            registrationType: "person", // Default one
            fio: null,
            edrpou: null,
            userSiteURL: null, //Optional
            phone: null,
            email: null,
            declarationEmail: null,
            providedServices: {
                "mounting": {
                name: "mounting",
                value: "Шиномонтаж",
                id: '0',
                isChecked: true
            }, 
            "carservice": {
                name: "carservice",
                value: "Автосервис",
                id: '1',
                isChecked: false
            }, 
            "shop": {
                name: "shop",
                value: "Магазин шин и дисков (в том числе точки на рынке)",
                id: '2',
                isChecked: false
            }, 
            "ecommerce": {
                name: "ecommerce",
                value: "Интернет-торговля",
                id: '3',
                isChecked: false
            }},
            address: null
        };
    }

    setUserDataForTesting() {
        return {
            registrationType: "person", // Default one
            fio: "lal",
            edrpou: "lal",
            userSiteURL: null, //Optional
            phone: "23432",
            email: "lal@lal.com",
            declarationEmail: "lal@lal.com",
            providedServices: {
                "mounting": {
                name: "mounting",
                value: "Шиномонтаж",
                id: '0',
                isChecked: true
            }, 
            "carservice": {
                name: "carservice",
                value: "Автосервис",
                id: '1',
                isChecked: true
            }, 
            "shop": {
                name: "shop",
                value: "Магазин шин и дисков (в том числе точки на рынке)",
                id: '2',
                isChecked: false
            }, 
            "ecommerce": {
                name: "ecommerce",
                value: "Интернет-торговля",
                id: '3',
                isChecked: false
            }},
            address: null
        };
    }

    resetUserActions() {
        this._$scope.registerForm.$setPristine();
        this.userData = this.setUserData();
        this.isUserAgreedCheck = false;
    }

    fileUpdated($files) {
        this.uploadedFilesLength = $files.length;
    }

    resetFiles() {
        this.uploadedFilesLength = 0;
        this.userData.files = null;
    }

    register() {
        this._$http({
            method: 'POST',
            url: '/user-approvement-request',
            headers: {
                'Content-Type': undefined
            },
            transformRequest: function(data) {
                let formData = new FormData();
                formData.append("model", angular.toJson(data.model));
                if (data.files && data.files.length > 0) {
                    for (let i = 0; i < data.files.length; i++) {
                        formData.append("file" + i, data.files[i]);
                    }
                }
                return formData;
            },
            data: {
                model: this.userData,
                files: this.userData.files
            }

        }).then(res => {
            this.Notification.success('Ваша заявка была отправлена!');
            this.resetUserActions();
            this.resetFiles();
            
            this._$timeout(() => {
                this._$scope.$apply();
            });
        }).catch(err => {
            this.resetUserActions();
            this.resetFiles();
            this.Notification.error(err.data || 'Возникла ошибка. Повторите попытку');
        });
    }
}