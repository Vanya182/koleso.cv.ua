'use strict';

import template from './register.html';

export default function routes($stateProvider) {
  $stateProvider
    .state('register', {
  		url: '/register',
      	template: template,
      	controller: 'registerController',
      	controllerAs: 'registerCtrl',
      	pageTitle: ''
    });
}
