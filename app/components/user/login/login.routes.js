'use strict';

import template from './login.html';

export default function routes($stateProvider) {
  $stateProvider
    .state('login', {
  		url: '/login',
      	template: template,
      	controller: 'loginController',
      	controllerAs: 'loginCtrl',
      	pageTitle: ''
    });
}
