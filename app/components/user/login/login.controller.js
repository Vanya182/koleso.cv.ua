'use strict';

export default class loginController {
	constructor(_fb, $state, $http, cfpLoadingBar, Notification, UserResources) {
		this._fb = _fb;
        this._$state = $state;
        this._$http = $http;
        this.cfpLoadingBar = cfpLoadingBar;
        this.Notification = Notification;
        this.UserResources = UserResources;
		this.credentials = {};
  	}	

    login(){
        this.cfpLoadingBar.start();
        this.UserResources.loginViaPassword(this.credentials).then(response => {
            this.Notification.success('Вы успешно вошли в систему');
            this._$state.go('home');
            this.cfpLoadingBar.complete();
        }).catch(err => {
            this.cfpLoadingBar.complete();
            this.Notification.error('Логин или пароль неверные');
        });
    }

    restorePassword(){
    	if (this.credentials.username){
            this._$http({
                method: 'POST',
                url: '/reset-password',
                data: {
                    email: this.credentials.username
                }
            }).then(res => {
               this.Notification.success('Новый пароль был выслан на Вашу почту')
            }).catch(err => {
                this.Notification.error(err || 'Возникла ошибка. Повторите попытку');
            });
        }else{
            this.Notification.error('Вы должны указать логин что бы восстановить пароль')
        }
    }
}