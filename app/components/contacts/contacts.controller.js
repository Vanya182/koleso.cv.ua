'use strict';

import {CONST} from 'core/app.constants';

export default class contactsController {
  constructor(NgMap) {
  	this.googleMapsUrl = `https://maps.googleapis.com/maps/api/js?key=${CONST.GOOGLE_MAP_KEY}`;
  	NgMap.getMap().then(function(map) {
    console.log(map.getCenter());
    console.log('markers', map.markers);
    console.log('shapes', map.shapes);
  });
  }
}