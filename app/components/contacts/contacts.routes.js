'use strict';

import template from './contacts.html';

export default function routes($stateProvider) {
  $stateProvider
    .state('contacts', {
  		url: '/contacts',
      	template: template,
      	controller: 'contactsController',
      	controllerAs: 'contactsCtrl',
      	pageTitle: 'Наши контакты'
    });
}
