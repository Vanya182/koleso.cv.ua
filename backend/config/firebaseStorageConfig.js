// Setting up storage bucket
const keyFilename = "./backend/config/firebase-service-key.json"; //replace this with api key file
const projectId = "koleso-cv-ua" //replace with your project id
const bucketName = `${projectId}.appspot.com`;

const gcs = require('@google-cloud/storage')({
    projectId,
    keyFilename
});

const bucket = gcs.bucket(bucketName);

const bucketRoutes = {
    approveRequest: 'user-approvement-request',
    users: 'users'
}


module.exports = {
	bucket,
	bucketName,
	bucketRoutes
}