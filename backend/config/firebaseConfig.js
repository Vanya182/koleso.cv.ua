// Init Firebase
const serviceAccount = require("./firebase-service-key.json");
const admin = require("firebase-admin");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://koleso-cv-ua.firebaseio.com"
});

module.exports = admin;