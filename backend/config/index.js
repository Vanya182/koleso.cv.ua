const express = require('express');
const path = require('path');
const routes = require(path.resolve('./backend/config/routes'));
const cors = require('cors');
const bodyParser = require('body-parser');

module.exports = function () {
    const app = express();
    app.use(cors());
    app.use(bodyParser.json()); // support json encoded bodies
    app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

    //create a cors middleware
    app.use((req, res, next) => {
        //set headers to allow cross origin request.
        res.header("Access-Control-Allow-Origin", "*");
        res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    //Setting up routes
    routes(app);

    // Showing stack errors
    app.set('showStackError', true);
    
    return app
};
