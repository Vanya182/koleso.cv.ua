const Q = require("Q");
const randomstring = require("randomstring");

// Requires firebase admin
const admin = require('./firebaseConfig.js');
// Requires storage bucket
const storage = require('./firebaseStorageConfig.js');

module.exports = {
    createPublicFileURL: storageName => {
        return `http://storage.googleapis.com/${storage.bucketName}/${encodeURIComponent(storageName)}`;
    },

    checkIfEmailExists(email) {
        let defered = Q.defer();
        let db = admin.database();
        let users = db.ref("users");
        users.once('value', snapshot => {
            snapshot.forEach(childSnapshot => {
                if (email === childSnapshot.val().email) {
                    defered.resolve(true);
                }
            })
            defered.resolve(false);
        });

        return defered.promise;
    },

    generateUserPassword() {
        return randomstring.generate()
    },

    deleteFolder(path) {
        return storage.bucket.deleteFiles({
            prefix: path
        });
    },

    uploadFiles(uploadDestination, filesArr) {
        let files = filesArr;
        let publicFileURLs = {};
        let uploadRoot = uploadDestination;
        /**
         * Storage upload part
         * TODO move file upload function in separate module
         */

        let uploadFile = (file, destination) => {
            let defered = Q.defer();

            let filePath = file.path;
            let uploadTo = destination;
            let fileMime = file.type;

            storage.bucket.upload(filePath, {
                destination: uploadTo,
                public: true,
                metadata: {
                    contentType: fileMime,
                    cacheControl: "public, max-age=300"
                }
            }).then(() => {
                defered.resolve(this.createPublicFileURL(uploadTo));
            }).catch(err => {
                defered.reject(err);
            })
            return defered.promise;
        }

        let promises = [];

    	if (files && Object.keys(files).length > 0){
    		Object.keys(files).map(key => {
	            let file = files[key];
	            let fileName = randomstring.generate() + file.name;
                let fileType = file.type;
	            /**
	             * Upload files to users folder
	             */
	            let uploadPromise = uploadFile(file, `${uploadRoot}/${fileName}`);
	            uploadPromise.then(publicFileURLs => {
	                return Q(publicFileURLs);
	            });
	            promises.push(uploadPromise);
	        })

	        return Q.all(promises);
    	}

        
    }
}