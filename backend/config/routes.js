const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const randomstring = require("randomstring");
const Q = require("Q");
const multipart = require('connect-multiparty');
const multipartyMiddleware = multipart();

// Requires mailing module
const mailer = require('./mailComponent');
// Requires firebase admin
const admin = require('./firebaseConfig.js');
const db = admin.database();
const ds = admin.firestore();

// Requires storage bucket
const storage = require('./firebaseStorageConfig.js');
// Requires common helpers functions
const helpers = require('./helpers');
// Requires google drive module
const googleDriveClass = require('./googleDrive.js');
const googleDrive = new googleDriveClass();




module.exports = function(app) {
    app.use('/', express.static(path.resolve('./')));

    app.use('/dist', express.static(path.resolve('./build')));

    app.use('/assets', express.static(path.resolve('./app/assets')));

    app.post('/user-approvement-request', multipartyMiddleware, (req, res) => {

        function generateRequest() {
            /**
             * New approvement request initial configuration
             */
            let requestsRef = db.ref("userApprovementRequests");
            let request = requestsRef.push();
            let requestID = request.key;
            let userFolderPath = `${storage.bucketRoutes.users}/${requestID}`;
            
            /**
            * Collect attached files if exist
            */
            let files = req.files;
            helpers.uploadFiles(userFolderPath, files)
                .then(publicFileURLs => {
                    approvementRecord.ID = requestID;
                    approvementRecord.files = publicFileURLs;
                    approvementRecord.userFolderPath = userFolderPath;
                    requestsRef.child(requestID).set(approvementRecord);
                    res.status(200).send();
                }).catch(err => {
                    console.error(err);
                    res.status(400).render(err);
                })
        }

        let approvementRecord = JSON.parse(req.body.model);

        helpers.checkIfEmailExists(approvementRecord.email).then(data => {
            if (data) {
                res.status(400).send("Этот email уже зарегистрирован в системе");
                return false;
            } else {
                generateRequest();
            }
        })

    });

    app.post('/create-user', (req, res) => {
        let userData = req.body;
        let userID = userData.ID;
        let userPassword = helpers.generateUserPassword();

        db.ref(`userApprovementRequests/${userID}`).once("value").then(data => {
            let attachedFiles = data.val().files;

            return admin.auth().createUser({
                    uid: userID,
                    email: userData.email,
                    emailVerified: true,
                    password: userPassword,
                    displayName: userData.email,
                    disabled: false
                })
                .then(userRecord => {
                    let registeredUsers = db.ref("users");
                    registeredUsers.child(userID).set(userData).then(res => {
                        // Send email with credentials to new user
                        mailer.sendEmailWithCredentials(userData.email, userPassword);
                    }).catch(() => {
                        console.error(error);
                        res.status(400).render(error);
                    })
                    res.status(200).send();
                })
                .catch(function(error) {
                    console.error(error);
                    res.status(400).render(error);
                });

        }).catch(err => {
            console.log(err);
        })
    })

    app.post('/delete-folder', (req, res) => {
        let userFolderPath = req.body.userFolderPath;

        helpers.deleteFolder(userFolderPath).then(data => {
            res.status(200).send();
        }).catch(err => {
            console.log(err);
            res.status(400).send();
        });
    });

    app.post('/upload-to-folder', multipartyMiddleware, (req, res) => {
        /**
        * Collect attached files if exist
        */
        let files = req.files;
        let uid = req.body.uid;
        let userFolderPath = `${storage.bucketRoutes.users}/${uid}`;

        helpers.uploadFiles(userFolderPath, files)
            .then(publicFileURLs => {
                res.status(200).send(publicFileURLs);
            }).catch(err => {
                console.error(err);
                res.status(400).render(err);
            });
    });

    app.post('/reset-password', (req, res) => {
        let userEmail = req.body.email;
        let newUserPassword = helpers.generateUserPassword();
        if (userEmail) {
            admin.auth().getUserByEmail(userEmail)
                .then(userRecord => {
                    let uid = userRecord.uid;
                    return uid;
                })
                .then(uid => {
                    return admin.auth().updateUser(uid, {
                        password: newUserPassword
                    })
                })
                .then(data => {
                    mailer.sendEmailWithCredentials(userEmail, newUserPassword);
                    res.status(200).send();
                })
                .catch(function(err) {
                    console.log(err);
                    res.status(400).send(err);
                });
        }
    })

    app.post('/delete-user', (req, res) => {
        let uid = req.body.uid;
        let userFolderPath = `${storage.bucketRoutes.users}/${uid}`;

        admin.auth().deleteUser(uid)
            .then(() => {
                let registeredUsers = db.ref("users");
                return registeredUsers.child(uid).set(null);
            })
            .then(() => {
                helpers.deleteFolder(userFolderPath)
            })
            .then(() => {
                res.status(200).send();
            })
            .catch(err => {
                res.status(400).send(err);
            });
    });

    app.post('/sync-db', (req, res) => {
        googleDrive.syncAll()
        .then(data => {
            let filteredData= [];

            // Loop through the imported files 
            data.map(fileData => {
                // Loop through rows-products
                fileData.map(item => {
                    // Filter each row-product
                    if (item.model && item.model.length > 0){
                        filteredData.push ({
                        'name': `${item.model} ${item.finish} et ${item.radius}`,
                        'model': item.model,
                        'diameter': item.diameter,
                        'finish': item.finish,
                        'height': item.height,
                        'instock': item.instock,
                        'priceretail': item.priceretail,
                        'pricewholesale': item.pricewholesale,
                        'radius': item.radius,
                        'spreading': item.spreading,
                        'thumburl': item.thumburl,
                        'width': item.width
                      })
                    }
                })
             });

            return filteredData;
        })
        .then(productsData => {
            ds.collection('products').add({
                first: "Ada",
                last: "Lovelace",
                born: 1815
            })
            .then(function(docRef) {
                console.log("Document written with ID: ", docRef.id);
            })
            .catch(function(error) {
                console.error("Error adding document: ", error);
            });

            return true;
            // let products = db.ref("products");
            // return products.set(productsData)
        })
        .then(() => {
            res.status(200).send();
        })
        .catch(err => {
            console.log(err);
            res.status(400).send(err);
        })
    })

    app.post('/send-mail', (req, res) => {
        // mailComponent.shareMail(req.body).then(()=>{
        //     res.status(200).send();
        // }).catch(()=>{
        //     res.status(400).send();
        // });
    });

    app.post('/support', multipartyMiddleware, (req, res) => {
        // mailComponent.sendSupportMail(req, req.body).then(()=>{
        //     res.status(200).send();
        // }).catch((err)=>{
        //     console.log(err);
        //     res.status(400).send();
        // });
    });
};