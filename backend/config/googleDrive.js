const Q = require("Q");
const GoogleSpreadsheet = require('google-spreadsheet');
const async = require('async');
// Get this from google developer console
const accessCredentials = require('./firebase-service-key.json');

const filesArr = ['1DzznjAqSzfp5xcbwuAbjpEqSodoHUChS4H9pmqf-Mks'];

class googleDrive{
  constructor(){
  }

  setAuth(file){
    let defered = Q.defer();

    file.useServiceAccountAuth(accessCredentials, err => {
      if (err) defered.reject(err);
      // Get all of the rows from the spreadsheet.
      // Get all of the rows from the spreadsheet.
      // 
      defered.resolve(true);
    });

    return defered.promise;
  }

  syncAll(){
    let result = [];
    let promises = [];

    filesArr.map(file => {
      let deferred = Q.defer();

      let fileRef = new GoogleSpreadsheet(file);
      
      let fileData = this.setAuth(fileRef)
        .then(res => {
          return Q(this.getInfoAndWorksheets(fileRef));
        })
      promises.push(fileData);

    })

    return Q.all(promises);
  }

  getInfoAndWorksheets(fileRef){
    let defered = Q.defer();

    fileRef.getInfo((err, info) => {
      if (err) console.log(err);
      let sheet = info.worksheets[0];

      // sheet.setHeaderRow(['', '', 'thumbURL', 'model', 'finish', 'finish', 'width', 'height', 'spreading', 'radius'
// , '', '', 'instock', 'priceWholesale', 'priceRetail']); 

      sheet.getRows({offset: 3}, (err, res) => {
        if (err){
          defered.reject(err);
        }
        defered.resolve(res);
      });

    });

    return defered.promise;
  }

}


module.exports = googleDrive;