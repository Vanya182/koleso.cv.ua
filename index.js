'use strict';

var app = require('./backend/config')();

var port = 3006;

app.listen(port, function () {
    console.log('Example app listening on port ' + port);
}); 
