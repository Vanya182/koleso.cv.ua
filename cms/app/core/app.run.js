'use strict';

export default function run($rootScope, $state, $stateParams, grant, UserResources) {
	$rootScope.$state = $state;
	$rootScope.$stateParams = $stateParams;

  $rootScope.$on('$stateChangeStart', function(event, toUrl, fromUrl) {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
  });

  /**
   * Ui-router grant module config
   * A test is very simple and takes two params.
   * @param  {String}     testName - A unique id for the test.
   * @param  {Function}   validate - A function that will validate whether your test passes or fails.
   */
  grant.addTest('logedIn', function() {
      return  UserResources.getUserToken();
  });
}