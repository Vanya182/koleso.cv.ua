'use strict';

export default function appConfig($qProvider, $provide, uiSelectConfig, NotificationProvider, cfpLoadingBarProvider) {
   $qProvider.errorOnUnhandledRejections(false);

    /* Angular-loading-bar config*/
        cfpLoadingBarProvider.includeSpinner = false;
        cfpLoadingBarProvider.includeBar = true;
  	/**
  	 * Angular ui-notification provider 
  	 */
  	 NotificationProvider.setOptions({
        delay: 10000,
        replaceMessage: true,
        startTop: 20,
        startRight: 20,
        verticalSpacing: 20,
        horizontalSpacing: 20,
        positionX: 'right',
        positionY: 'top'
    });

  	/**
  	 * Angular ui select2 theme settings
  	 */
	uiSelectConfig.theme = 'select2';

	
}