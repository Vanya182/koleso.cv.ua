'use strict'

export default class HomeController {
  constructor($state, $scope, $timeout, UserResources, Notification) {
        this._$state = $state;
        this._$scope = $scope;
        this._$timeout = $timeout;
        this.UserResources = UserResources;
        this.Notification = Notification;

  }
  
  logout() {
        this.UserResources.clearUser().then(() => {
            this._$state.go('login');
            this.Notification.success('Вы успешно вышли из системы');
        }).catch(err => {
            this.Notification.error(err);
        });
    }
}

