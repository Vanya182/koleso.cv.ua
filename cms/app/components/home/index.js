import angular from 'angular';
import uirouter from 'angular-ui-router/release/angular-ui-router.min.js';

import routing from './home.routes';
import homeController from './home.controller';

export default angular.module('kolesoCMS.home', [uirouter])
	.config(routing)
	.controller('homeController', homeController)
	// We need this as we need to export module name for angular DI
	.name;