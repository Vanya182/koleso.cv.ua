'use strict';

import template from './home.html';

export default function routes($stateProvider) {
  $stateProvider
    .state('home', {
    	abstract: true,
      	url: '',
      	template: template,
      	controller: 'homeController',
      	controllerAs: 'homeCtrl',
      	// resolve: {
       //                  logedIn: grant => {
       //                      return grant.only({test: 'logedIn', state: 'login' });
       //                  }
       //              }
    });
}
