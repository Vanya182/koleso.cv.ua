'use strict'

export default class globalSettingsController {
	constructor(_fb, $http, Notification, cfpLoadingBar) {
		this._fb = _fb;
		this._$http = $http;
		this.Notification = Notification;
        this.cfpLoadingBar = cfpLoadingBar;
	}

	updateDB(){
		this.cfpLoadingBar.start();

		this._$http({
            method: 'POST',
            url: '/sync-db'
        }).then(res => {
        	console.log(res);
            this.Notification.success('База была обновлена'); 
        }).catch(err => {
            this.Notification.error('Возникла ошибка при импортировании данных. Проверьте форматирование импортируемых документов');
        });
	}
}

