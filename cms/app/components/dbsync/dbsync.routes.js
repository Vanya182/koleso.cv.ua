'use strict';

import template from './dbsync.html';
import controller from './dbsync.controller';

export default function routes($stateProvider) {
  $stateProvider
    .state('home.dbsync', {
      	url: '/sync',
      	template: template,
      	controller: controller,
      	controllerAs: 'dbsyncCtrl',
      	resolve: {
            logedIn: (grant) => {
                return grant.only({test: 'logedIn', state: 'login' });
            }
        }
    });
}
