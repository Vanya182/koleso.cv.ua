import angular from 'angular';
import uirouter from 'angular-ui-router/release/angular-ui-router.min.js';

import routing from './dbsync.routes';
import dbsyncController from './dbsync.controller';

export default angular.module('kolesoCMS.dbsync', [uirouter])
	.config(routing)
	.controller('dbsyncController', dbsyncController)
	// We need this as we need to export module name for angular DI
	.name;