'use strict';

export default class loginController {
	constructor(_fb, $state, cfpLoadingBar, Notification, UserResources) {
		this._fb = _fb;
        this._$state = $state;
        this.cfpLoadingBar = cfpLoadingBar;
        this.Notification = Notification;
        this.UserResources = UserResources;
		this.credentials = {};
        this.authUsersRef = this._fb.getAuthUsers();
  	}	

    login(){
         this.authUsersRef.on('value', data => {
            this.cfpLoadingBar.start();
            let authUsers = Object.keys(data.val()).map(key => {return key});
            this.UserResources.loginViaPassword(this.credentials).then(response => {
                let userUid = response.uid;
                if (authUsers.indexOf(userUid) >= 0){
                    this.Notification.success('Вы успешно вошли в систему');
                    this._$state.go('home.globalSettings');
                }else{
                    this.UserResources.clearUser();
                    this.Notification.error('Логин или пароль введены неверно');
                }
                this.cfpLoadingBar.complete();
            }).catch(err => {
                this.cfpLoadingBar.complete();
                this.Notification.error('Такой пользователь не зарегистрирован в системе');
            });
         }, err=>{
                this.cfpLoadingBar.complete();
                this.Notification.error(err);
            })
    }
}