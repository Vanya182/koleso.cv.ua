'use strict';

import angular from 'angular';
import uirouter from 'angular-ui-router/release/angular-ui-router.min.js';

/**
 * Page login
 */
import loginRouting from './login/login.routes';
import loginController from './login/login.controller';

export default angular.module('kolesoCMS.user', [])
	.config(loginRouting)
	.controller('loginController', loginController)

	// We need this as we need to export module name for angular DI
	.name;