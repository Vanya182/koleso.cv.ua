'use strict';

import template from './registered-users.html';

export default function routes($stateProvider) {
  $stateProvider
    .state('home.registeredUsers', {
      	url: '/registered-users',
      	template: template,
      	controller: 'registeredUsersController',
      	controllerAs: 'registeredUsersCtrl',
      	resolve: {
                        logedIn: grant => {
                            return grant.only({test: 'logedIn', state: 'login' });
                        }
                    }
    });
}
