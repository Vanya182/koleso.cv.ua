import angular from 'angular';
import uirouter from 'angular-ui-router/release/angular-ui-router.min.js';


import routing from './registered-users.routes';
import registeredUsersController from './registered-users.controller';

import userInfoRouting from './registered-user-info/registered-user-info.routes';
import registeredUserInfoController from './registered-user-info/registered-user-info.controller';

export default angular.module('kolesoCMS.registeredUsers', [uirouter])
	.config(routing)
	.controller('registeredUsersController', registeredUsersController)

	.config(userInfoRouting)
	.controller('registeredUserInfoController', registeredUserInfoController)
	// We need this as we need to export module name for angular DI
	.name;