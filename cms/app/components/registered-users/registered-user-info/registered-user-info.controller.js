'use strict'

import swal from 'sweetalert';

export default class registeredUserInfoController {
    constructor(_fb, $state, $scope, $stateParams, $http, $timeout, cfpLoadingBar, Notification) {
        this._fb = _fb;
        this.cfpLoadingBar = cfpLoadingBar;
        this._$state = $state;
        this._$scope = $scope;
        this._$timeout = $timeout;
        this._$stateParams = $stateParams;
        this._$http = $http;
        this.Notification = Notification;
        this.model = {
            user: null
        };
        this.userID = this._$stateParams.userID;
        this.getUserData();
    }

    getUserData() {
        this.cfpLoadingBar.start();
        this._fb.getRegisteredUserData(this.userID).once('value').then(data => {
            this.model.user = data.val();
            console.log(this.model.user);
            this.cfpLoadingBar.complete();
        }).catch(err => {
            this.cfpLoadingBar.complete();
            this.Notification.error(err);
        })
    }

    deleteUser() {
        if (this.userID) {
            swal({
                title: "Вы уверены?",
                text: "Вы не сможете восстановить удаленные данные!"
            }).then(isConfirmed => {
                if (isConfirmed) {
                    this._$http({
                        method: 'POST',
                        url: '/delete-user',
                        data: {
                            uid: this.userID
                        }
                    }).then(res => {
                            this.Notification.success('Учетная запись пользователя была удалена');
                            this._$state.go('home.registeredUsers');
                    }).catch(err => {
                        this._$state.go('home.registeredUsers');
                        this.Notification.error('Возникла ошибка');
                    });
                } else {
                    return false
                }
            })
        }
    }
}