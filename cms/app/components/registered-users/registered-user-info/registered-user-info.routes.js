'use strict';

import template from './registered-user-info.html';

export default function routes($stateProvider) {
  $stateProvider
    .state('home.registeredUserInfo', {
      	url: '/registered-users/:userID',
      	template: template,
      	controller: 'registeredUserInfoController',
      	controllerAs: 'registeredUserInfoCtrl',
      	resolve: {
            logedIn: grant => {
                return grant.only({test: 'logedIn', state: 'login' });
            }
        }
    });
}
