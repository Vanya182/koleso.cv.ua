'use strict'

export default class registeredUsersController {
	constructor(_fb, cfpLoadingBar, Notification) {
		this._fb = _fb;
		this.cfpLoadingBar = cfpLoadingBar;
		this.Notification = Notification;
		this.model = {
			users: null
		}

		this.getRegisteredUsers();
	}

	getRegisteredUsers() {
	    this.cfpLoadingBar.start();
	    this._fb.getRegisteredUsers().once('value').then(data => {
	        this.model.users = data.val();
	        this.cfpLoadingBar.complete();
	    }).catch(err => {
	        this.cfpLoadingBar.complete();
	        this.Notification.error(err);
	    })
	}
}

