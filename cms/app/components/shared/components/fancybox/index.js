'use strict';

require('fancybox')($);

export function fancyboxConfig() {
	return {
		restrict: 'A',
    	link: (scope, element, attrs) => {
	      $(element).fancybox({  
	        'overlayShow' : false,
            'cyclic'    :   false,
            'showNavArrows' :   false,
            "type": "image"
	      });
	    }
	}
};

   
