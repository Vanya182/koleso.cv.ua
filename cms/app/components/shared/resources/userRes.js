'use strict'

export default class userRes {
    constructor(_fb) {
        this._fb = _fb;
    }

    getUser() {
        return firebase.auth().currentUser;
    };

    isUserAdmin() {
        return new Promise((resolve, reject) => {
            if (this.getUserToken()) {
                this._fb.getAuthUsers().on('value', data => {
                    let authUsers = Object.keys(data.val()).map(key => {
                        return key
                    });
                    let userUid = this.getUser().uid;
                    if (authUsers.indexOf(userUid) >= 0) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                }, err => {
                    reject(err);
                });
            } else {
                resolve(false);
            }
        });
    }

    loginViaPassword(credentials) {
        return firebase.auth().signInWithEmailAndPassword(credentials.username, credentials.password);
    };

    authWithToken(token) {
        alert(token);
        return firebase.auth().signInWithCustomToken(token);
    }

    registerUser(userData) {
        return firebase.auth().createUserWithEmailAndPassword(userData.email, userData.password);
    };

    clearUser() {
        return firebase.auth().signOut();
    };

    getUserToken() {
        let localStorageKeys = Object.keys(localStorage);
        // Looking for needed local storage firebase auth key
        let subString = "firebase:authUser";
        for (let i = 0; i < localStorageKeys.length; i++) {
            if (localStorageKeys[i].indexOf(subString) > -1)
                return JSON.parse(localStorage.getItem(localStorageKeys[i])).uid;
        }
        return null
    };

    setUserToken(token) {
        return localStorage.setItem('authToken', token);
    };
}