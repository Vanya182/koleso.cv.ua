'use strict';

export default class firebaseRes{
    constructor(_fbRoutes){
        this._fbRoutes = _fbRoutes;
    }
    
    /**
     * Database part
     */
    
    getUserApprovementRequests(){
        return firebase.database().ref(this._fbRoutes.userApprovementRequests);
    }

    getUserApprovementRequest(requetID){
        return firebase.database().ref(`${this._fbRoutes.userApprovementRequests}/${requetID}`);
    }

    getGlobalSettings(){
        return firebase.database().ref(this._fbRoutes.globalSettings);
    }
    
    getAuthUsers(){
        return firebase.database().ref(this._fbRoutes.oIDs);
    }

    getRegisteredUsers(){
        return firebase.database().ref(this._fbRoutes.users);
    }

    getRegisteredUserData(userID){
        return firebase.database().ref(`${this._fbRoutes.users}/${userID}`);
    }

    getProducts(){
        return firebase.database().ref(this._fbRoutes.products);
    }
}

