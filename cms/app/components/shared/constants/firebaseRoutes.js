'use strict';

export const _fbRoutes = {
	userApprovementRequests: 'userApprovementRequests',
	globalSettings: 'globalSettings',
    oIDs: 'oIDs',
    users: 'users',
    products: 'products'
};

