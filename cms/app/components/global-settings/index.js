import angular from 'angular';
import uirouter from 'angular-ui-router/release/angular-ui-router.min.js';

import routing from './global-settings.routes';
import globalSettingsController from './global-settings.controller';

export default angular.module('kolesoCMS.globalSettings', [uirouter])
	.config(routing)
	.controller('globalSettingsController', globalSettingsController)
	// We need this as we need to export module name for angular DI
	.name;