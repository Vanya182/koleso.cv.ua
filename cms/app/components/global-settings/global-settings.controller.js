'use strict'

export default class globalSettingsController {
	constructor(_fb, Notification, cfpLoadingBar) {
		this._fb = _fb;
		this.Notification = Notification;
        this.cfpLoadingBar = cfpLoadingBar;
		this.model = {
			social: {
				vk: null,
				facebook: null,
				instagram: null,
				youtube: null
			}
		}
		this.getModel();
	}

	getModel(){
        this.cfpLoadingBar.start();
		this._fb.getGlobalSettings().once('value').then(data => {
			this.model = data.val();
	        this.cfpLoadingBar.complete();
		}).catch(err => {
	        this.cfpLoadingBar.complete();
			this.Notification.error(err);
		})
	}

	updateModel(){
        this.cfpLoadingBar.start();
		this._fb.getGlobalSettings().set(this.model).then(res => {
			this.Notification.success('Изменения были сохранены');
	        this.cfpLoadingBar.complete();
		}).catch(err => {
	        this.cfpLoadingBar.complete();
			this.Notification.error(err);
		});
	}
}

