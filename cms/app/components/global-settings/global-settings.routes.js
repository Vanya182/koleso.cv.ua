'use strict';

import template from './global-settings.html';

export default function routes($stateProvider) {
  $stateProvider
    .state('home.globalSettings', {
      	url: '/',
      	template: template,
      	controller: 'globalSettingsController',
      	controllerAs: 'globalSettingsCtrl',
      	resolve: {
            logedIn: (grant) => {
                return grant.only({test: 'logedIn', state: 'login' });
            }
        }
    });
}
