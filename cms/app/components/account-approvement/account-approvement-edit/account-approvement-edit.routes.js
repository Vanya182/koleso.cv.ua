'use strict';

import template from './account-approvement-edit.html';

export default function routes($stateProvider) {
  $stateProvider
    .state('home.accountApprovementEdit', {
      	url: '/account-approvemet/:requestID',
      	template: template,
      	controller: 'accountApprovementEditController',
      	controllerAs: 'accountApprovementEditCtrl',
      	resolve: {
            logedIn: grant => {
                return grant.only({test: 'logedIn', state: 'login' });
            }
        }
    });
}
