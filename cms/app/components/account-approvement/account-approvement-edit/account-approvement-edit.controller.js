'use strict'

export default class accountApprovementEditController {
    constructor(_fb, $state, $scope, $stateParams, $http, $timeout, cfpLoadingBar, Notification) {
        this._fb = _fb;
        this.cfpLoadingBar = cfpLoadingBar;
        this._$state = $state;
        this._$scope = $scope;
        this._$timeout = $timeout;
        this._$stateParams = $stateParams;
        this._$http = $http;
        this.Notification = Notification;
        this.model = {
            request: null
        };
        this.requestID = this._$stateParams.requestID;
        this.getRequestData();         
    }

    getRequestData() {
        this.cfpLoadingBar.start();
        this._fb.getUserApprovementRequest(this.requestID).once('value').then(data => {
            this.model.request = data.val();
            this.cfpLoadingBar.complete();
        }).catch(err => {
            this.cfpLoadingBar.complete();
            this.Notification.error(err);
        })
    }

    approveRequest() {
        this.cfpLoadingBar.start();
        this._$http({
            method: 'POST',
            url: '/create-user',
            data: this.model.request
        }).then(res => {
            this._fb.getUserApprovementRequest(this.requestID).set(null).then(res => {
                this.Notification.success('Учетная запись была создана'); 
                this._$state.go('home.accountApprovement');
            })
        }).catch(err => {
            this._$state.go('home.accountApprovement');
            this.Notification.error('Возникла ошибка');
        });
    }

    rejectRequest() {
        this._fb.getUserApprovementRequest(this.requestID).set(null).then(res => {
            /**
             * Remove attached location images if provided
             */
            this._$http({
                method: 'POST',
                url: '/delete-folder',
                data: {
                    userFolderPath: this.userFolderPath
                }
            }).then(res => {
                this._$state.go('home.accountApprovement');
                this.Notification.success('Запрос был отклонен');
            }).catch(err => {
                this._$state.go('home.accountApprovement');
                this.Notification.error('Возникла ошибка');
            });
          
        }).catch(err => {
            this.Notification.error(err);
        });
    }


}