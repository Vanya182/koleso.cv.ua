import angular from 'angular';
import uirouter from 'angular-ui-router/release/angular-ui-router.min.js';

import routing from './account-approvement.routes';
import accountApprovementController from './account-approvement.controller';

import editRouting from './account-approvement-edit/account-approvement-edit.routes';
import accountApprovementEditController from './account-approvement-edit/account-approvement-edit.controller';


export default angular.module('kolesoCMS.accountApprovement', [uirouter])
	.config(routing)
	.controller('accountApprovementController', accountApprovementController)

	.config(editRouting)
	.controller('accountApprovementEditController', accountApprovementEditController)
	// We need this as we need to export module name for angular DI
	.name;