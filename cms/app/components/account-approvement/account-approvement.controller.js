'use strict'

export default class accountApprovementController {
    constructor(_fb, cfpLoadingBar, Notification) {
    	this._fb = _fb;
    	this.cfpLoadingBar = cfpLoadingBar;
    	this.Notification = Notification;
    	this.model = {
    		requests: null
    	};
        this.getRequests();
    }

    getRequests() {
        this.cfpLoadingBar.start();
        this._fb.getUserApprovementRequests().once('value').then(data => {
            this.model.requests = data.val();
            this.cfpLoadingBar.complete();
        }).catch(err => {
            this.cfpLoadingBar.complete();
            this.Notification.error(err);
        })
    }

}