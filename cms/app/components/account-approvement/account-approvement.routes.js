'use strict';

import template from './account-approvement.html';

export default function routes($stateProvider) {
  $stateProvider
    .state('home.accountApprovement', {
      	url: '/account-approvemet',
      	template: template,
      	controller: 'accountApprovementController',
      	controllerAs: 'accountApprovementCtrl',
      	resolve: {
                        logedIn: grant => {
                            return grant.only({test: 'logedIn', state: 'login' });
                        }
                    }
    });
}
