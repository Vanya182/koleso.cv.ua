'use strict';

/**
 * App dependencies
 */
import * as angular from "angular";
import uirouter from 'angular-ui-router/release/angular-ui-router.min.js';
import uiSelect from 'ui-select';
import angularSanitize from 'angular-sanitize';
// https://github.com/alexcrack/angular-ui-notification
import notification from 'angular-ui-notification';
import angularLoadingBar from 'angular-loading-bar';
import uiRouterGrant from 'ui-router.grant';
// import 'ng-sweet-alert';


/**
 * App config
 */
import run from 'core/app.run';
import config from 'core/app.config';
import routes from 'core/app.routes';

/**
 * App components
 */
import home from 'components/home';
import user from 'components/user';
import globalSettings from 'components/global-settings';
import accountApprovement from 'components/account-approvement';
import registeredUsers from 'components/registered-users';
import dbsync from 'components/dbsync';

/**
 * App constants
 */
import {_fbRoutes} from 'components/shared/constants/firebaseRoutes';
/**
 * App shared components
 */
import {headerConfig} from 'components/shared/components/header';
import {footerConfig} from 'components/shared/components/footer';
import {imgAsyncConfig} from 'components/shared/components/img-async';
import {fancyboxConfig} from 'components/shared/components/fancybox';

/**
 * App shared resources
 */
import userRes from 'components/shared/resources/userRes';
import firebaseRes from 'components/shared/resources/firebaseRef';
/**
 * App shared filters
 */
import sceTrustAsHtmlConfig from 'components/shared/filters/sceTrustAsHtml';

/**
 * App vendor
 */


/**
 * App bootstraping
 */
angular
	.module('kolesoCMS', [
		/**
		 * Dependencies
		 */
		uirouter,
		'ui.router.grant',
		uiSelect,
		angularSanitize,
		notification,
		angularLoadingBar,
		/**
		 * App components
		 */
		home,
		globalSettings,
		accountApprovement,
		registeredUsers,
		user,
		dbsync
	])
	.config(config)
	.config(routes)
	.run(run)
	.constant('_fbRoutes', _fbRoutes)
	.component('headerComponent', headerConfig)
	.component('footerComponent', footerConfig)
	/**
	 * Thi one is a directive not a component as we want to 
	 * avoid extra component wrapper element 
	 */
	.directive('imgAsyncComponent', imgAsyncConfig)
	.directive('fancybox', fancyboxConfig)
	.filter('sceTrustAsHtml', sceTrustAsHtmlConfig)
	.service('UserResources', userRes)
	.service('_fb', firebaseRes)

